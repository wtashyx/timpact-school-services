#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
   @author: Emerson Andrade
   @since: September 7, 2019
   @website: mrson dot com
"""


from selenium import webdriver

import pandas as pd
import os
from time import sleep

CHROMEDRIVER_PATH_NAME = "Chromium/chromedriver_for_linux"
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
DRIVER_BIN = os.path.join(PROJECT_ROOT, CHROMEDRIVER_PATH_NAME)

def scrape(ID_user, NIP_user):
    """
    This function access the Student Information System and create a csv
    file with the Anahuac's student informations in the report folder.
    """
    if os.name != "nt":
        options = webdriver.ChromeOptions()
        options.binary_location = "/usr/bin/chromium"

    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument("--remote-debugging-port=9222")
    options.add_argument('--disable-dev-shm-usage')
    
    browser = webdriver.Chrome(executable_path = DRIVER_BIN, chrome_options=options)
    browser.get('https://rua-ssb-prod.ec.lcred.net/pls/UAN/twbkwbis.P_WWWLogin') # Login

    ID = browser.find_element_by_xpath('//*[@id="UserID"]').send_keys(ID_user) # ID number
    NIP = browser.find_element_by_xpath('//*[@id="PIN"]/input').send_keys(NIP_user) # NIP number
    Button = browser.find_element_by_xpath('/html/body/div[3]/form/p/input').click() # Enviar
    print(browser.current_url)
    browser.get('https://rua-ssb-prod.ec.lcred.net/pls/UAN/twbkwbis.P_GenMenu?name=bmenu.P_StuMainMnu') # Servicios al Alumno
    sleep(0.5)
    try:
        for button_xpath in ['/html/body/div[3]/table[1]/tbody/tr[2]/td[2]/a/b', # Información Académica
                        '/html/body/div[3]/table[1]/tbody/tr[3]/td[2]/a/b', # Consulta de Historia Académica
                        '/html/body/div[3]/form/input']: # Enviar
            Button = browser.find_element_by_xpath(button_xpath).click()
            sleep(0.5)
    except:
        return False


    string_table = browser.find_element_by_xpath('/html/body/div[3]/p/table[1]') # informations table
    string_table = string_table.text.split('\n')
    string_table = pd.DataFrame(string_table)
    string_table.to_csv('Reports/info_anahuac_'+ID_user+'.csv') # saving
    browser.close()
    
def read_scrape(ID_user):
    """
    This function read the csv file from the Anahuac's student in the report folder and
    return a list with informations about School, Major, Courses, Grades, etc.
    """

    fields = []
    courses = []
    levels = []
    grades = []
    main_info = ['Tipo de Alumno:', 'Escuela:', 'Carrera:']
    
    file = pd.read_csv('Reports/info_anahuac_'+ID_user+'.csv')
    data = dict(zip(file.columns, range(len(file.columns))))
    data_stack = file.rename(columns=data).stack()

    # finding the main info
    for field in main_info:
        field_stack = (data_stack.str.startswith(field)==True).idxmax()
        main_info[main_info.index(field)]=field+' '+file.iloc[field_stack].replace(field,'').strip()

    # finding the subjects
    bool_series = file["0"].str.startswith('Materia', na = False)
    grade_list = []
    for i in range(len(file[bool_series])):
        index_subject = int(file[bool_series].iloc[i,0])
        counter=1
        while counter<=50:
            try:
                string_subject = file.iloc[index_subject+counter,1].strip()
                grade_subject = eval(string_subject[-3:])
                if len(string_subject.split())>5 and isinstance(grade_subject,float)==True and grade_subject<=10.0 and grade_subject>=0.0 and string_subject not in grade_list:
                    string_subject_strip = string_subject.strip()
                    string_subject_split = string_subject.split(" ")
                    fields.append(string_subject_split[0])
                    courses.append(string_subject_split[1])
                    for level_subject in string_subject_split:
                        if len(level_subject)==2 and level_subject in ["LC","PM","MA"]:
                            levels.append(level_subject)
                    grades.append(string_subject_split[-1])
                    grade_list.append(string_subject)
            except:
                pass
            counter += 1

    # calculating the fields' averages
    all_fields = []
    average_fields = {}
    for field_i in range(len(fields)):
        if fields[field_i] not in all_fields:
            all_fields.append(fields[field_i])
    for field_j in all_fields:
        average = 0.0
        for field_k in range(len(fields)):
            if fields[field_k]==field_j:
                if average==0.0:
                    average = float(grades[field_k])
                else:
                    average = (average+float(grades[field_k]))/2.0
        average_fields[field_j]=round(average,3)

    student_data = [main_info, [fields, courses, levels, grades], [all_fields, average_fields]]
    return student_data
