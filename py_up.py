#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
   @author: Emerson Andrade
   @since: October 6, 2019
   @website: mrson dot com
"""

from time import sleep
from selenium import webdriver

import pandas as pd
import os

CHROMEDRIVER_PATH_NAME = "Chromium/chromedriver_for_windows.exe" if os.name == "nt" else "Chromium/chromedriver_for_linux"
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
DRIVER_BIN = os.path.join(PROJECT_ROOT, CHROMEDRIVER_PATH_NAME)

def scrape(ID_user, PASS_user):
    """
    This function access the Student Information System and create a csv
    file with the UP's student informations in the report folder.
    """
    if os.name != "nt":
        options = webdriver.ChromeOptions()
        options.binary_location = "/usr/bin/chromium"

    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument("--remote-debugging-port=9222")
    options.add_argument('--disable-dev-shm-usage')

    browser = webdriver.Chrome(executable_path = DRIVER_BIN)
    browser.get('https://www.upsite.upmx.mx/psp/Campus/?cmd=login&languageCd=ESP&') # Login

    ID = browser.find_element_by_xpath('//*[@id="userid"]').send_keys(ID_user) # ID number
    NIP = browser.find_element_by_xpath('//*[@id="pwd"]').send_keys(PASS_user) # PASS
    Button = browser.find_element_by_xpath('//*[@id="login"]/div/div/div[8]/input').click() # Enviar

    browser.get('https://www.upsite.upmx.mx/psp/Campus/EMPLOYEE/HRMS/c/SA_LEARNER_SERVICES.SSS_MY_CRSEHIST.GBL?PORTALPARAM_PTCNAV=HC_SSS_MY_CRSEHIST_GBL&EOPP.SCNode=HRMS&EOPP.SCPortal=EMPLOYEE&EOPP.SCName=HCCC_ACADEMIC_RECORDS&EOPP.SCLabel=Registros%20Acad%c3%a9micos&EOPP.SCPTfname=HCCC_ACADEMIC_RECORDS&FolderPath=PORTAL_ROOT_OBJECT.CO_EMPLOYEE_SELF_SERVICE.HCCC_ACADEMIC_RECORDS.HC_SSS_MY_CRSEHIST_GBL&IsFolder=false') # Mi Historial de Cursos
    browser.switch_to.frame(browser.find_element_by_id("ptifrmtgtframe"))
    string_table = browser.find_element_by_xpath('//*[@id="CRSE_HIST$scroll$0"]/tbody') # informations table
    string_table = string_table.text.split('\n')
    sleep(2)
    
    browser.get('https://www.upsite.upmx.mx/psc/Campus/EMPLOYEE/HRMS/c/SA_LEARNER_SERVICES.SSS_MY_ACAD.GBL?Page=SSS_MY_ACAD&Action=U') # Mi Datos Academicos
    string_table2 = browser.find_element_by_xpath('//*[@id="win0divDERIVED_SSSACAD_SSR_GROUP5"]/table') # informations table
    string_table2 = string_table2.text.split('\n')

    string_table_list = []
    for s in string_table: string_table_list.append(s)
    for s2 in string_table2: string_table_list.append(s2)
    
    string_table = pd.DataFrame(string_table_list)
    string_table.to_csv('Reports/info_up_'+ID_user+'.csv') # saving

    browser.close()

def read_scrape(ID_user):
    """
    This function read the csv file from the UP's student in the report folder and
    return a list with informations about School, Major, Courses, Grades, etc.
    """

    fields = []
    courses = []
    levels = []
    grades = []
    main_info = ['  Grado -', '  Institución -', '  Prog -']
    
    file = pd.read_csv('Reports/info_up_'+ID_user+'.csv')
    data = dict(zip(file.columns, range(len(file.columns))))
    data_stack = file.rename(columns=data).stack()

    # finding the main info
    for field in main_info:
        field_stack = (data_stack.str.startswith(field)==True).idxmax()
        main_info[main_info.index(field)]=field.replace("-","").strip()+': '+str(file.iloc[field_stack]).replace(field,'').strip()+' - '+str(file.iloc[(field_stack[0]+3,1)]).replace(field,'').strip()

    # finding the subjects
    bool_series = file["0"].str.startswith('Curso', na = False)
    grade_list = []
    for i in range(len(file[bool_series])):
        index_subject = int(file[bool_series].iloc[i,0])
        counter=1
        while counter<=5000:
            try:
                string_subject = str(file.iloc[index_subject+counter,1]).strip()
                grade_subject = eval(str(file.iloc[index_subject+counter+2,1]))
                units = eval(str(file.iloc[index_subject+counter+3,1]))
                if len(string_subject.split())>1:
                    if isinstance(grade_subject,float)==True and grade_subject<=10.0 and grade_subject>=0.0 and units>=0 and string_subject not in grade_list:
                        field_subject = str(file.iloc[index_subject+counter-1,1]).strip()
                        field_subject = field_subject.split(" ")
                        if len(field_subject)>1:
                            course_subject = field_subject[-1]
                            field_subject = field_subject[-1][:-3]
                        fields.append(field_subject) # e.g. FIN, ADM
                        courses.append(course_subject) # e.g. 2203, 1201
                        grades.append(grade_subject)
                        grade_list.append(string_subject)
            except:
                pass
            counter += 1

    # calculating the fields' averages
    all_fields = []
    average_fields = {}
    for field_i in range(len(fields)):
        if fields[field_i] not in all_fields:
            all_fields.append(fields[field_i])
    for field_j in all_fields:
        average = 0.0
        for field_k in range(len(fields)):
            if fields[field_k]==field_j:
                if average==0.0:
                    average = float(grades[field_k])
                else:
                    average = (average+float(grades[field_k]))/2.0
        average_fields[field_j]=round(average,3)

    student_data = [main_info, [fields, courses, grades], [all_fields, average_fields]]
    return student_data
                