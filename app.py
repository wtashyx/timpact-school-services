import py_anahuac as Anahuac
import py_ebc as Ebc
import py_up as UPan
import plots
from flask import Flask, escape, request, jsonify
from time import sleep
import model_runner_ as MRunner

app = Flask(__name__)


@app.route('/', methods=['GET'])
def home():
    return jsonify({'message':"hello world"}), 200

@app.route("/getBestScores", methods=["POST"])
def plotsHandler():
    data = request.json
    result = plots.Info_Calif_Plot(data.get("matricula"), data.get("user_id"));
    return jsonify({ "message" : "success" }), 200

@app.route("/runScore", methods=["POST"])
def runScore():
    data = request.json
    result = MRunner.Final_Function(data.get("user_id"), data.get("matricula"))
    return jsonify({ "message": "success", "data": "ok" }), 200


@app.route('/marks/transform', methods=["POST", "GET"])
def getAnahuac():
    if request.method != "POST":
        return jsonify({ "message": "error", "data": "method not allowed" }), 405
    
    data = request.json

    matricula = data.get("matricula")
    password = data.get("password")
    school_id = data.get("school_id")

    schools = {
        1: Anahuac,
        4: Ebc,
        5: UPan
    }
    
    SCHOOL_NAME = schools.get(school_id)
    if(SCHOOL_NAME == None):
        return jsonify({ "message": "error", "data": "school id not found" }), 404
    do_scrape = SCHOOL_NAME.scrape(matricula,password)
    if(do_scrape == False):
        return jsonify({ "message": "error", "data": "user has retentions" }), 403
    
    sleep(2)
    read_scrape = SCHOOL_NAME.read_scrape(matricula)
    

    return jsonify({ "message": "ok", "data": "data obtained successfully" }), 200


if __name__ == '__main__':
    app.run(debug=True, host= '0.0.0.0')
