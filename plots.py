import pandas as pd
import random
import numpy as np
from sqlalchemy              import create_engine

def Info_Calif_Plot(nombre,user_id):
    try:
        df_anahuac = pd.read_csv('./Reports/info_anahuac_'+str(nombre)+'.csv')

        lst_calif_prev = [x for x in df_anahuac['0'].tolist(
        ) if 'ADM' in x or 'ECO' in x or 'FIN' in x or 'HUM' in x or 'MAT' in x or 'CON' in x or 'LDR' in x]

        ADM_LIST = [x for x in df_anahuac['0'].tolist() if 'ADM' in x]
        ECO_LIST = [x for x in df_anahuac['0'].tolist() if 'ECO' in x]
        FIN_LIST = [x for x in df_anahuac['0'].tolist() if 'FIN' in x]
        HUM_LIST = [x for x in df_anahuac['0'].tolist() if 'HUM' in x]
        MAT_LIST = [x for x in df_anahuac['0'].tolist() if 'MAT' in x]
        CON_LIST = [x for x in df_anahuac['0'].tolist() if 'CON' in x]
        LDR_LIST = [x for x in df_anahuac['0'].tolist() if 'LDR' in x]

        df_anahuac = pd.DataFrame({x[33:-3]: [x[-3:]] for x in lst_calif_prev})
        df_anahuac_ADM = pd.DataFrame({x[33:-3]: [x[-3:]] for x in ADM_LIST})
        df_anahuac_ECO = pd.DataFrame({x[33:-3]: [x[-3:]] for x in ECO_LIST})
        df_anahuac_FIN = pd.DataFrame({x[33:-3]: [x[-3:]] for x in FIN_LIST})
        df_anahuac_HUM = pd.DataFrame({x[33:-3]: [x[-3:]] for x in HUM_LIST})
        df_anahuac_MAT = pd.DataFrame({x[33:-3]: [x[-3:]] for x in MAT_LIST})
        df_anahuac_CON = pd.DataFrame({x[33:-3]: [x[-3:]] for x in CON_LIST})
        df_anahuac_LDR = pd.DataFrame({x[33:-3]: [x[-3:]] for x in LDR_LIST})

        df_anahuac = df_anahuac.applymap(lambda x: function_aux(x))
        df_anahuac_ADM = df_anahuac_ADM.applymap(lambda x: function_aux(x))
        df_anahuac_ECO = df_anahuac_ECO.applymap(lambda x: function_aux(x))
        df_anahuac_FIN = df_anahuac_FIN.applymap(lambda x: function_aux(x))
        df_anahuac_HUM = df_anahuac_HUM.applymap(lambda x: function_aux(x))
        df_anahuac_MAT = df_anahuac_MAT.applymap(lambda x: function_aux(x))
        df_anahuac_CON = df_anahuac_CON.applymap(lambda x: function_aux(x))
        df_anahuac_LDR = df_anahuac_LDR.applymap(lambda x: function_aux(x))

        Media_ADM = round(df_anahuac_ADM.values[0].mean(), 0)
        Media_ECO = round(df_anahuac_ECO.values[0].mean(), 0)
        Media_FIN = round(df_anahuac_FIN.values[0].mean(), 0)
        Media_HUM = round(df_anahuac_HUM.values[0].mean(), 0)
        Media_MAT = round(df_anahuac_MAT.values[0].mean(), 0)
        Media_CON = round(df_anahuac_CON.values[0].mean(), 0)
        Media_LDR = round(df_anahuac_LDR.values[0].mean(), 0)

        df_prom_areas = pd.DataFrame([[user_id,Media_ADM, Media_ECO, Media_FIN, Media_HUM, Media_MAT, Media_CON,
                                       Media_LDR]], columns=['user_id','ADMINISTRACION', 'ECONOMIA', 'FINANZAS',
                                                             'HUMANIDADES', 'MATEMATICAS', 'CONTABILIDAD','LIDERAZGO'])
    except Exception as e:
        print(e)
        list1 = [6.0, 7.0, 8.0, 9.0]
        df_prom_areas = pd.DataFrame([[user_id,random.sample(list1, 1)[0], random.sample(list1, 1)[0], random.sample(list1, 1)[0], random.sample(list1, 1)[0], random.sample(list1, 1)[0], random.sample(list1, 1)[0],
                                       random.sample(list1, 1)[0]]], columns=['user_id','ADMINISTRACION', 'ECONOMIA', 'FINANZAS',
                                                                           'HUMANIDADES', 'MATEMATICAS', 'CONTABILIDAD','LIDERAZGO'])
    
    
    string_conn_aws_pgsql_prod_l = 'postgresql://timpactAccess:27JTMavN4RZ4DWG8GJ6kjj7h@35.192.61.65/timpact'
    pgsql_engine_l               = create_engine(string_conn_aws_pgsql_prod_l)
    print("ok here")
    print(df_prom_areas)
    try:
        conn = pgsql_engine_l.connect()
        conn.execute('delete from df_prom_areas where user_id = %d' %user_id)
        df_prom_areas.to_sql('df_prom_areas', conn, if_exists='append', index=False)
        conn.close()
        pgsql_engine_l.dispose()
    except Exception as e:
        print(e)
        conn.close()
        pgsql_engine_l.dispose()
    return True
