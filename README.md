# Timpact Services

This repository mainly contains Python Scripts that range from a Flask web app to the model that produces the Timpact 
###### The py_ebc.py does the scrapping on the EBC webpage. For more information, contact Emerson
###### The py_anahuac.py does the scrapping on the Anahuac webpage. For more information, contact Emerson
###### The py_up.py does the scrapping on the UP webpage. For more information, contact Emerson
###### The app.py runs a function on the model_runner.py which generates the score by three different methods, the first one tries to generate it by taking the school scores, the PAP and the job experience.
###### If it fails because of the job experience, it runs with the PAP and the school scores.
###### Finally, if it doesn't work either, it generates the score with the PAP only.

##
---
##
###### It is recommended to run the whole project on virtual env. 
##
---

### Route (/)
###### A healthcheck, returns a trivial message on JSON format

### Route (/getBestScores)
###### POST. Requires two params. The matricula which can be found on the database as well as the user_id.

### Route (/runScore)
###### POST. Requires two params, the user_id and the matricula. It runs a function on the model runner and writes to the database the score and other stuff.

### Route (/marks/transform)
###### POST. Requires three arguments. Requires matricula, password and school\_id. The scrapper (done by Emerson) will go to the school page (any of the py_[school name] and get the school scores.
##
---