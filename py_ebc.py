#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
   @author: Emerson Andrade
   @since: October 6, 2019
   @website: mrson dot com
"""

from time import sleep
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import pandas as pd
import os

CHROMEDRIVER_PATH_NAME = "Chromium/chromedriver_for_windows.exe" if os.name == "nt" else "Chromium/chromedriver_for_linux"
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
DRIVER_BIN = os.path.join(PROJECT_ROOT, CHROMEDRIVER_PATH_NAME)

def scrape(ID_user, PASS_user):
    """
    This function access the Student Information System and create a csv
    file with the EBC's student informations in the report folder.
    """

    if os.name != "nt":
        options = webdriver.ChromeOptions()
        options.binary_location = "/usr/bin/chromium"


    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument("--remote-debugging-port=9222")
    
    browser = webdriver.Chrome(executable_path = DRIVER_BIN, chrome_options=options)
    browser.get('https://intranet.ebc.edu.mx/') # Login

    browser.switch_to.frame(browser.find_element_by_name("cas-login_iframe"))
    ID = browser.find_element_by_xpath('//*[@id="username"]').send_keys(ID_user) # User
    PASS = browser.find_element_by_xpath('//*[@id="password"]').send_keys(PASS_user) # Password
    Button = browser.find_element_by_xpath('//*[@id="botonSumitLogin"]').click() # Enviar

    sleep(0.5)
    Button = browser.find_element_by_xpath('//*[@id="fila-1"]/div[5]/a/div/div/div/div[2]/h3').click() # Historial Académico I
    sleep(0.5)
    browser.switch_to.frame(0)
    Button = browser.find_element_by_xpath('/html/body/div[3]/form/table/tbody/tr[2]/td[2]/a').click() # Historial Académico II
    sleep(0.5)

    string_table = browser.find_element_by_xpath('/html/body/div[3]') # informations table
    string_table = string_table.text.split('\n')
    string_table = pd.DataFrame(string_table)
    string_table.to_csv('Reports/info_ebc_'+ID_user+'.csv') # saving

    browser.close()

def read_scrape(ID_user):
    """
    This function read the csv file from the EBC's student in the report folder and
    return a list with informations about School, Major, Courses, Grades, etc.
    """

    fields = []
    courses = []
    levels = []
    grades = []
    main_info = ['Programa:', 'Nivel:', 'Escuela:', 'Carreras:']
    
    file = pd.read_csv('Reports/info_ebc_'+ID_user+'.csv')
    data = dict(zip(file.columns, range(len(file.columns))))
    data_stack = file.rename(columns=data).stack()

    # finding the main info
    for field in main_info:
        field_stack = (data_stack.str.startswith(field)==True).idxmax()
        main_info_string = str(file.iloc[field_stack]).replace(field,'').strip()
        if field=='Programa:':
            main_info_string = main_info_string.split(' ')
            main_info_string = main_info_string[:main_info_string.index('Periodo')]
            main_info_string = ' '.join(main_info_string)
        if field=='Escuela:':
            main_info_string = main_info_string.split(' ')
            main_info_string = main_info_string[:main_info_string.index('Promedio')]
            main_info_string = ' '.join(main_info_string)
        main_info[main_info.index(field)]=field+' '+main_info_string

    # finding the subjects
    bool_series = file["0"].str.startswith('Materia', na = False)
    grade_list = []
    for i in range(len(file[bool_series])):
        index_subject = int(file[bool_series].iloc[i,0])
        counter=1
        while counter<=50:
            string_subject = file.iloc[index_subject+counter,1].strip()
            if len(string_subject.split())>2:
                try:
                    grade_subject = float(file.iloc[index_subject+counter+1,1])
                    if isinstance(grade_subject,float)==True and grade_subject<=10.0 and grade_subject>=0.0 and string_subject not in grade_list:
                        string_subject_strip = string_subject.strip()
                        string_subject_split = string_subject.split(" ")
                        fields.append(string_subject_split[0]) # e.g. FIN, ADM
                        courses.append(string_subject_split[1]) # e.g. 2203, 1201
                        grades.append(grade_subject)
                        grade_list.append(string_subject)
                except:
                    pass
                
            counter += 1

    # calculating the fields' averages
    all_fields = []
    average_fields = {}
    for field_i in range(len(fields)):
        if fields[field_i] not in all_fields:
            all_fields.append(fields[field_i])
    for field_j in all_fields:
        average = 0.0
        for field_k in range(len(fields)):
            if fields[field_k]==field_j:
                if average==0.0:
                    average = float(grades[field_k])
                else:
                    average = (average+float(grades[field_k]))/2.0
        average_fields[field_j]=round(average,3)

    student_data = [main_info, [fields, courses, levels, grades], [all_fields, average_fields]]
    return student_data
