#!/usr/bin/env python
# coding: utf-8

# In[482]:


import pandas as pd
import random
import numpy  as np
import pickle

from sqlalchemy              import create_engine
from sklearn.linear_model    import LogisticRegression 
from sklearn.model_selection import train_test_split
from sklearn.metrics         import roc_auc_score,accuracy_score
from sklearn.preprocessing   import StandardScaler

pd.set_option('display.max_columns',1000)
pd.set_option('display.max_rows',1000)


# # Function PAP 

# In[483]:


def Score_Only_PAP(ID, MATRICULA):
    
    string_conn_aws_pgsql_prod_l = 'postgresql://timpactAccess:27JTMavN4RZ4DWG8GJ6kjj7h@35.192.61.65/timpact'
    pgsql_engine_l               = create_engine(string_conn_aws_pgsql_prod_l)

    try:
        conn = pgsql_engine_l.connect()

        query_1 = 'select * from pap_result where user_id =%d'%ID
        query_2 = 'select * from cat_pap_terms'
        query_3 = 'select * from user_data'

        pap_result    = pd.read_sql(query_1,conn)
        cat_pap_terms = pd.read_sql(query_2,conn)
        user_data = pd.read_sql(query_3,conn)

        dict_id_term       = {k:v for k,v in zip(cat_pap_terms.id,cat_pap_terms.term)}
        dict_id_definition = {k:v for k,v in zip(cat_pap_terms.id,cat_pap_terms.definition)}

        df_skills               = pap_result.sort_values(by='pap_value',ascending=False)[:5][['user_id','pap_term_id','pap_value']].reset_index(drop=True)
        df_skills['definition'] = df_skills.pap_term_id.apply(lambda x:dict_id_definition[x])

        df_oppor                = pap_result.sort_values(by='pap_value',ascending=False)[-5:][['user_id','pap_term_id','pap_value']].reset_index(drop=True)
        df_oppor['definition']  = df_oppor.pap_term_id.apply(lambda x:dict_id_definition[x])

        conn.execute('delete from df_skills where user_id = %d' %ID)
        conn.execute('delete from df_oppor where user_id = %d' %ID)
        df_skills.to_sql('df_skills', conn, if_exists='append', index=False)
        df_oppor.to_sql ('df_oppor' , conn, if_exists='append', index=False)

        ##############
        ###Modeling###
        ##############

        df_to_evaluate               = pd.merge(pap_result,cat_pap_terms,left_on='pap_term_id',right_on='id')[['pap_value','term']].set_index('term').T.reset_index(drop=True)
        df_to_evaluate.columns.names = ['']
        modelo                       = pickle.load(open('modelo_logreg.pkl', "rb"))
        std_scaler                   = pickle.load(open('StandardScaler.pkl','rb'))
        Xs                           = pd.DataFrame(std_scaler.transform(df_to_evaluate),columns=df_to_evaluate.columns)
        df_score                     = pd.DataFrame()
        df_score['user_id']          = [ID]
        df_score['score']            = [int(1000*(modelo.predict_proba(Xs)[:,1][0]))]
        print(df_score)
        
        df_salar                     = pd.DataFrame()
        df_salar['user_id']          = [ID]
        df_salar['salar']            = np.random.randint(15000,20000,1)

        ############################
        ###Uploading Model Result###
        ############################

        conn.execute('delete from df_score where user_id = %d' %ID)
        df_score.to_sql('df_score', conn, if_exists='append', index=False)

        conn.close()
        pgsql_engine_l.dispose
        
        return([df_to_evaluate,df_score,df_skills,df_oppor,df_salar])

    except Exception as e:

        print('Error : ',e)
        conn.close()
        pgsql_engine_l.dispose()


# # Function Anahuac-PAP

# In[484]:


def Score_Anahuac_PAP(ID, MATRICULA):
    
    string_conn_aws_pgsql_prod_l = 'postgresql://timpactAccess:27JTMavN4RZ4DWG8GJ6kjj7h@35.192.61.65/timpact'
    pgsql_engine_l               = create_engine(string_conn_aws_pgsql_prod_l)

    try:
        conn = pgsql_engine_l.connect()
        
        df_skills = pd.read_sql('select * from df_skills where user_id = %d'%ID,conn)
        df_oppor = pd.read_sql('select * from df_oppor where user_id = %d'%ID,conn)

        df_anahuac = pd.read_csv('./Reports/info_anahuac_{MATRICULA}.csv')
        print('Score only PAP')
        df_to_evaluate          = Score_Only_PAP(ID)[0]

        lst_calif_prev          = [x for x in df_anahuac['0'].tolist() if 'ADM' in x or 'ECO' in x or 'FIN' in x or 'HUM' in x or 'MAT' in x or 'CON' in x or 'FIN' in x or 'LDR' in x]

        df_anahuac              = pd.DataFrame({x[33:-3]:[x[-3:]] for x in lst_calif_prev})
                     
        df_anahuac              =  df_anahuac.applymap(lambda x:function_aux(x))

        df_anahuac_pap          = pd.concat([df_anahuac,df_to_evaluate],axis=1)
        
        modelo_pap_anahuac      = pickle.load(open('modelo_logreg_an_pap.pkl', "rb"))
        std_scaler_pap_anahuac  = pickle.load(open('StandardScaler_an_pap.pkl','rb'))

        Xs                      = pd.DataFrame(std_scaler_pap_anahuac.transform(df_anahuac_pap),columns=df_anahuac_pap.columns)
        df_score                = pd.DataFrame()
        df_score['user_id']     = [ID]
        df_score['score']       = [int(1000*(modelo.predict_proba(Xs)[:,1][0]))]
        
        df_salar                     = pd.DataFrame()
        df_salar['user_id']          = [ID]
        df_salar['salar']            = np.random.randint(15000,20000,1)
        
        print('Score Anahuac-PAP')
        print(df_score)

        conn.execute('delete from df_score where user_id = %d' %ID)
        df_score.to_sql('df_score', conn, if_exists='append', index=False)

        conn.close()
        pgsql_engine_l.dispose
        
        return([df_anahuac_pap,df_score,df_skills,df_oppor,df_salar])
    
    except Exception as e:
        
        print('Error : ',e)
        conn.close()
        pgsql_engine_l.dispose


# # Function Anahuac_PAP_Salario 

# In[485]:


def Score_Anahuac_PAP_SEMCOT(ID, MATRICULA):

    string_conn_aws_pgsql_prod_l = 'postgresql://timpactAccess:27JTMavN4RZ4DWG8GJ6kjj7h@35.192.61.65/timpact'
    pgsql_engine_l               = create_engine(string_conn_aws_pgsql_prod_l)

    try:
        conn = pgsql_engine_l.connect()
        
        df_skills = pd.read_sql('select * from df_skills where user_id = %d'%ID,conn)
        df_oppor = pd.read_sql('select * from df_oppor where user_id = %d'%ID,conn)

        df_to_evaluate = Score_Anahuac_PAP(ID)[0]
        df_to_evaluate['Semanas_Cotizadas'] = np.random.randint(1,400,1)

        modelo_pap_anahuac_semcot      = pickle.load(open('modelo_logreg_an_pap_semcot.pkl', "rb"))
        std_scaler_pap_anahuac_semcot  = pickle.load(open('StandardScaler_an_pap_semcot.pkl','rb'))

        Xs = pd.DataFrame(std_scaler_pap_anahuac_semcot.transform(df_to_evaluate),columns=df_to_evaluate.columns)

        df_score                     = pd.DataFrame()
        df_score['user_id']          = [ID]
        df_score['score']            = [int(1000*(modelo_pap_anahuac_semcot.predict_proba(Xs)[:,1][0]))]
        print(df_score)
        
        df_salar                     = pd.DataFrame()
        df_salar['user_id']          = [ID]
        df_salar['salar']            = np.random.randint(15000,20000,1)
        

        ############################
        ###Uploading Model Result###
        ############################

        conn.execute('delete from df_score where user_id = %d' %ID)
        df_score.to_sql('df_score', conn, if_exists='append', index=False)

        conn.close()
        pgsql_engine_l.dispose()
        
        return([df_score,df_skills,df_oppor,df_salar])

    except Exception as e:

        print('Error : ',e)
        conn.close()
        pgsql_engine_l.dispose()


# # General Function 

# In[507]:


def General_Function(ID, MATRICULA):
    
    results = Score_Only_PAP(ID, MATRICULA)

    result = Score_Anahuac_PAP(ID, MATRICULA)

    results = Score_Anahuac_PAP_SEMCOT(ID, MATRICULA)

    return result

def Emergency(ID, MATRICULA):

    string_conn_aws_pgsql_prod_l = 'postgresql://timpactAccess:27JTMavN4RZ4DWG8GJ6kjj7h@35.192.61.65/timpact'
    pgsql_engine_l               = create_engine(string_conn_aws_pgsql_prod_l)

    try:
        conn = pgsql_engine_l.connect()

        query_1 = 'select * from df_score order by random() limit 1'
        query_2 = 'select * from df_skills order by random() limit 5'
        query_3 = 'select * from df_oppor  order by random() limit 5'

        df_score = pd.read_sql(query_1,conn)
        df_sk  = pd.read_sql(query_2,conn)
        df_op  = pd.read_sql(query_3,conn)

    except Exception as e:

        conn.close()
        pgsql_engine_l.dispose()

    df_sk['user_id'] = ID
    df_op['user_id'] = ID

    df_sk.sort_values(by='pap_value').reset_index(drop=True)
    df_op.sort_values(by='pap_value').reset_index(drop=True)

    df_score.reset_index(drop=True)
    df_score['user_id']          = [ID]
    print(df_score)

    df_salar                     = pd.DataFrame()
    df_salar['user_id']          = [ID]
    df_salar['salar']            = np.random.randint(15000,20000,1)
    
    conn.execute('delete from df_score where user_id = %d' %ID)
    conn.execute('delete from df_skills where user_id = %d' %ID)
    conn.execute('delete from df_oppor where user_id = %d' %ID)
    df_score.to_sql('df_score', conn, if_exists='append', index=False)
    df_sk.to_sql('df_skills', conn, if_exists='append', index=False)
    df_op.to_sql('df_oppor', conn, if_exists='append', index=False)

    return([df_score,df_op,df_sk,df_salar])


# In[508]:


def Final_Function(ID, MATRICULA):
    try:
        General_Function(ID, MATRICULA)
    except:
        Emergency(ID)


# # Modelo_PAP

# df_pap = pd.merge(pap_result[:40],cat_pap_terms,left_on='pap_term_id',right_on='id')[['pap_value','term']].set_index('term').T.reset_index(drop=True)
# df_pap.columns.names = ['']
# 
# ###Creación DataFrame Simulado para crear una regresion logisitica provisional
# ### en lo que se obtienen los datos reales del dia 31 de Octubre-Anahuac
# 
# df_pap_sim = pd.DataFrame()
# 
# for x in df_pap.columns:
#     df_pap_sim[x] = np.random.randint(0,100,500)
# 
# df_pap_sim = pd.concat([df_pap,df_pap_sim]).reset_index(drop=True)
# 
# var = [x for x in df_pap_sim.columns if x!='target']
# df_pap_sim['target'] = [np.random.randint(0,2,1)[0] for x in range(len(df_pap_sim))]
# 
# X = df_pap_sim[var].copy()
# y = df_pap_sim['target']
# 
# sc = StandardScaler()
# sc.fit(X)
# Xs = pd.DataFrame(sc.transform(X),columns=X.columns)
# 
# Xt, Xv, yt, yv = train_test_split(Xs,y,train_size=0.7)
# 
# Xt.shape,Xv.shape,yt.shape,yv.shape
# 
# modelo = LogisticRegression()
# 
# modelo.fit(Xt,yt)
# 
# #pickle.dump(modelo , open("modelo_logreg.pkl", "wb"))
# #pickle.dump(sc , open('StandardScaler.pkl','wb'))
# 
# print (roc_auc_score(y_true=yt,y_score=modelo.predict_proba(Xt)[:,1]))
# print (roc_auc_score(y_true=yv,y_score=modelo.predict_proba(Xv)[:,1]))
# 
# print (accuracy_score(y_true=yt,y_pred=modelo.predict(Xt)))
# print (accuracy_score(y_true=yv,y_pred=modelo.predict(Xv)))

# # Modelo_PAP_Anahuac 

# df_anahuac = pd.read_csv('info_anahuac_00150678.csv')
# 
# df_to_evaluate = Score_Only_PAP(1)
# 
# lst_calif_prev = [x for x in df_anahuac['0'].tolist() if 'ADM' in x or 'ECO' in x or 'FIN' in x or 'HUM' in x or 'MAT' in x or 'CON' in x or 'FIN' in x or 'LDR' in x]
# 
# df_anahuac =pd.DataFrame({x[33:-3]:[x[-3:]] for x in lst_calif_prev})
# 
# def function_aux(value):
#     try:
#         output_value = float(value)
#     except:
#         output_value = 0.0
# 
#     return output_value
# 
# df_anahuac =  df_anahuac.applymap(lambda x:function_aux(x))
# 
# df_anahuac_sim = pd.DataFrame()
# 
# for x in df_anahuac.columns:
#     df_anahuac_sim[x] = np.random.randint(0,10,500)
# 
# df_pap_sim = pd.DataFrame()
# 
# for x in df_to_evaluate.columns:
#     df_pap_sim[x] = np.random.randint(0,100,500)
# 
# df_anahuac_pap_sim = pd.concat([df_anahuac_sim,df_pap_sim],axis=1)
# 
# df_anahuac_pap = pd.concat([df_anahuac,df_to_evaluate],axis=1)
# 
# df_anahuac_pap_sim = pd.concat([df_anahuac_pap,df_anahuac_pap_sim]).reset_index(drop=True)
# 
# df_anahuac_pap_sim['target'] = [np.random.randint(0,2,1)[0] for x in range(len(df_anahuac_pap_sim))]
# var = [x for x in df_anahuac_pap_sim.columns if x!='target']
# 
# X = df_anahuac_pap_sim[var].copy()
# y = df_anahuac_pap_sim['target']
# print(X.shape)
# 
# sc = StandardScaler()
# sc.fit(X)
# Xs = pd.DataFrame(sc.transform(X),columns=X.columns)
# 
# Xt, Xv, yt, yv = train_test_split(Xs,y,train_size=0.7)
# 
# Xt.shape,Xv.shape,yt.shape,yv.shape
# 
# modelo = LogisticRegression()
# 
# modelo.fit(Xt,yt)
# 
# pickle.dump(modelo , open("modelo_logreg_an_pap.pkl", "wb"))
# pickle.dump(sc , open('StandardScaler_an_pap.pkl','wb'))
# 
# print (roc_auc_score(y_true=yt,y_score=modelo.predict_proba(Xt)[:,1]))
# print (roc_auc_score(y_true=yv,y_score=modelo.predict_proba(Xv)[:,1]))
# 
# print (accuracy_score(y_true=yt,y_pred=modelo.predict(Xt)))
# print (accuracy_score(y_true=yv,y_pred=modelo.predict(Xv)))

# # Modelo_PAP_Anahuac_Salario

# df_anahuac = pd.read_csv('info_anahuac_00150678.csv')
# 
# df_to_evaluate = Score_Only_PAP(6)
# 
# lst_calif_prev = [x for x in df_anahuac['0'].tolist() if 'ADM' in x or 'ECO' in x or 'FIN' in x or 'HUM' in x or 'MAT' in x or 'CON' in x or 'FIN' in x or 'LDR' in x]
# 
# df_anahuac =pd.DataFrame({x[33:-3]:[x[-3:]] for x in lst_calif_prev})
# 
# def function_aux(value):
#     try:
#         output_value = float(value)
#     except:
#         output_value = 0.0
# 
#     return output_value
# 
# df_anahuac =  df_anahuac.applymap(lambda x:function_aux(x))
# 
# df_anahuac_sim = pd.DataFrame()
# 
# for x in df_anahuac.columns:
#     df_anahuac_sim[x] = np.random.randint(0,10,500)
# 
# df_pap_sim = pd.DataFrame()
# 
# for x in df_to_evaluate.columns:
#     df_pap_sim[x] = np.random.randint(0,100,500)
# 
# df_anahuac_pap_sim = pd.concat([df_anahuac_sim,df_pap_sim],axis=1)
# 
# df_anahuac_pap = pd.concat([df_anahuac,df_to_evaluate],axis=1)
# 
# df_anahuac_pap_sim_sem = pd.concat([df_anahuac_pap,df_anahuac_pap_sim]).reset_index(drop=True)
# 
# df_anahuac_pap_sim_sem['Semanas_Cotizadas'] = np.random.randint(1,400,501)
# 
# df_anahuac_pap_sim_sem['target'] = [np.random.randint(0,2,1)[0] for x in range(len(df_anahuac_pap_sim_sem))]
# var = [x for x in df_anahuac_pap_sim_sem.columns if x!='target']
# 
# X = df_anahuac_pap_sim_sem[var].copy()
# y = df_anahuac_pap_sim_sem['target']
# 
# sc = StandardScaler()
# sc.fit(X)
# Xs = pd.DataFrame(sc.transform(X),columns=X.columns)
# 
# Xt, Xv, yt, yv = train_test_split(Xs,y,train_size=0.7)
# 
# Xt.shape,Xv.shape,yt.shape,yv.shape
# 
# modelo = LogisticRegression()
# 
# modelo.fit(Xt,yt)
# 
# #pickle.dump(modelo , open("modelo_logreg_an_pap_semcot.pkl", "wb"))
# #pickle.dump(sc     , open('StandardScaler_an_pap_semcot.pkl','wb'))
# 
# print (roc_auc_score(y_true=yt,y_score=modelo.predict_proba(Xt)[:,1]))
# print (roc_auc_score(y_true=yv,y_score=modelo.predict_proba(Xv)[:,1]))
# 
# print (accuracy_score(y_true=yt,y_pred=modelo.predict(Xt)))
# print (accuracy_score(y_true=yv,y_pred=modelo.predict(Xv)))

# # General Function 

# In[ ]:




